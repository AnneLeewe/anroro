package com.example.die_dreisten_drei.an2ro.backend_gedoens;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.example.die_dreisten_drei.an2ro.MainActivity;
import com.example.die_dreisten_drei.an2ro.R;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.EventBus.MessageEvent;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import org.greenrobot.eventbus.EventBus;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */

public class RSSUpdateContentHandler implements ContentHandler {

    private Podcast tempcast;
    private StringBuilder currentValue = new StringBuilder();
    private boolean inItem = false;
    private PodcastItem podcastItem;
    private boolean needUpdate = true;

    public RSSUpdateContentHandler(Podcast x) {
        tempcast = x;
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
        if (needUpdate) {
            if (localName.equals("item")) {
                inItem = true;
                podcastItem = new PodcastItem();
            }

            if (localName.equals("enclosure")) {
                podcastItem.setPath(atts.getValue("url"));
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        String tempString = currentValue.toString().trim();
        if (tempString.contains("<") && tempString.contains(">")) {
            tempString = tempString.replaceAll("<br/>", "\n\n").replaceAll("\\<.*?\\>", "");
        }
        currentValue.setLength(0);

        if (needUpdate) {

            if (localName.equals("lastBuildDate")) {
                if (tempcast.getDate().equals(tempString)) {
                    needUpdate = false;
                } else {
                    tempcast.setDate(tempString);
                }
            }

            if (localName.contains("duration")) {
                podcastItem.setDuration(tempString);
            }

            if (localName.contains("author")) {
                if (inItem) {
                    podcastItem.setAuthor(tempString);
                }
            }

            if (localName.contains("description")) {
                if (inItem) {
                    podcastItem.setDescription(tempString);
                }
            }

            if (localName.equals("title")) {
                if (inItem) {
                    podcastItem.setTitle(tempString);
                }
            }

            if (localName.equals("item")) {
                inItem = false;
                boolean itemexist = false;
                for (PodcastItem x : tempcast.getStreams()) {
                    if (x.getPath().equals(podcastItem.getPath()) || x.getOld_url() != null && x.getOld_url().equals(podcastItem.getPath())) {
                        itemexist = true;
                    }
                }
                if (!itemexist) {
                    podcastItem.setPodcast_image(tempcast.getImage());
                    tempcast.getStreams().add(podcastItem);
                }
            }
        }
    }

    @Override
    public void characters(char[] chars, int start, int length) throws SAXException {
        currentValue.append(chars, start, length);
    }

    @Override
    public void startDocument() throws SAXException {

    }

    @Override
    public void endDocument() throws SAXException {
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(new MainActivity(), DatabaseHelper.class);
        final RuntimeExceptionDao<Podcast, Integer> podcastDAO = databaseHelper.getPodcastDao();
        if (needUpdate)
            podcastDAO.update(tempcast);
        EventBus.getDefault().post(new MessageEvent("RSSend"));
    }

    @Override
    public void startPrefixMapping(String s, String s1) throws SAXException {
    }

    @Override
    public void endPrefixMapping(String s) throws SAXException {
    }

    @Override
    public void ignorableWhitespace(char[] chars, int i, int i1) throws SAXException {
    }

    @Override
    public void processingInstruction(String s, String s1) throws SAXException {
    }

    @Override
    public void skippedEntity(String s) throws SAXException {
    }

    @Override
    public void setDocumentLocator(Locator locator) {
    }

}
