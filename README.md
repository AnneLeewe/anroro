# AnRoRo

Mobile Systeme SS2018 Gruppenprojekt
~~Die aktuellste Version stellt die Branch "itsTheFinalMergeDown" dar. 
Dies ist eine Anspielung auf die Band Europe und nicht auf den mentalen Zusammenbruch unsererseits, 
den atomaren Zusammenbruch eines Kernkraftwerks in Tschernobyl noch hat es was mit Ausnutzen von Sicherheitslücken in CPUs zu tun.~~

## Installation
Die App ist auf den SDK-Versionen 23 - 27 lauffähig.

Da dieses Projekt nicht den Anspruch hegt, im Google Play Store erhältlich zu sein, 
wird die App über die "run"-Funktion (Umschalt + F10) von Android Studio auf ein Android Gerät aufgespielt.

Um .apk-Dateien (Android Package Kit) auf das Smartphone übertragen zu können, muss der Entwicklermodus eingestellt sein.

(Anleitung Entwicklermodus: https://mobilsicher.de/schritt-fuer-schritt/usb-debugging-aktivieren)

Danach kann die App genau so genutzt werden als ob sie über den Play Store herunter geladen wurde. 