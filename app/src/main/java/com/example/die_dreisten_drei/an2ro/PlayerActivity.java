package com.example.die_dreisten_drei.an2ro;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.die_dreisten_drei.an2ro.backend_gedoens.EventBus.MessageEvent;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.EventBus.SeekbarEvent;

import com.example.die_dreisten_drei.an2ro.backend_gedoens.Podcast;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.PlayerService;

//Superclass --> Bindingclass
import com.example.die_dreisten_drei.an2ro.backend_gedoens.PlayerService.MyLocaleBinder;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.PodcastItem;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */
//ToDo: PlayerInit lädt den Podcast schon im Vorfeld, sollte er erst bei Play machen?
//ToDo: Next / Prev Podcast
public class PlayerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    /*BackgroundServ for MediaPlayer*/
    static boolean isPlaying = false;

    int currentPosition;
    int currentLength;
    PodcastItem podcastItem;
    final static String MY_ACTION = "MY_ACTION";
    final static String INVALID_URL = "INVALID_URL";
    private PlayerService PlayerServiceObj;
    boolean isBound = false;
    private IntentFilter receiveFilter;
    public NavigationView navigationView;
    private DrawerLayout mDrawerLayout;


    private Podcast podCast;
    private int podcastItemPosition = 0;
    private int PODCAST_LISTPOSITION=0;
    private String url;

    private ImageButton btnPlay;
    private ImageButton btnPause;
    private ImageButton btnDownload;
    private ImageButton btnSave;
    private ImageButton btnSkipPrev;
    private ImageButton btnSkipNext;
    private MediaPlayer mediaPlayer;
    private TextView tfPlayTime;
    private TextView tfTitle;
    private ImageView imageIcon;
    private TextView tfNextTitle;
    private TextView tfNextDesc;
    private SeekBar seekBar;
    private TextView tfDescription;
    private Intent i;

    private boolean seekingPodcast;

    String podcastLaenge = "";
    String podcastCurrentPosi = "";
    int intPodcastCurrentPosi = 0;
    int intPodcastLaenge = 0;

    Handler seekHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.baseline_menu_white_18dp);

        navigationView = (NavigationView) findViewById(R.id.nav_view3);
        navigationView.setNavigationItemSelectedListener(this);

        mDrawerLayout = findViewById(R.id.player_drawer_layout);

        Intent intent = getIntent();
        PODCAST_LISTPOSITION=(int) intent.getSerializableExtra("PODCAST");
        podCast = MainActivity.aboList.get(PODCAST_LISTPOSITION);
        podcastItemPosition = intent.getIntExtra("ITEMPOSITION", 0);
        url = podCast.getStreams().get(podcastItemPosition).getPath();


//        podcastItem = MainActivity.aboList.get(PODCAST_LISTPOSITION).getStreams().get(podcastItemPosition);
        podcastItem = podCast.getStreams().get(podcastItemPosition);

        /*Intent für den PlayerService*/
        i = new Intent(PlayerActivity.this, PlayerService.class);
//Intent service, danach muss ein ServiceConnection-Objekt sein, danach Flag setzen
//
// Wegen der Flag konnte der Service nie beendet werden, siehe hier: https://stackoverflow.com/a/25643975
        bindService(i, PlayerServiceConnection, 0);
        //Intent service, danach muss ein ServiceConnection-Objekt sein, danach Flag setzen
        //Wegen der Flag konnte der Service nie beendet werden, siehe hier: https://stackoverflow.com/a/25643975
        bindService(i, PlayerServiceConnection, 0);


        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(INVALID_URL));

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter(MY_ACTION));

        initializeViews();

       
            startService();


        setupSeekbar();

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();

    }



    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(PlayerServiceConnection!=null && isBound == true) {

            unbindService(PlayerServiceConnection);
            isBound = false;
        }
        isBound = false;

    }


    public void startService() {

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(PlayerService.MY_ACTION);

        //Start our own service
        Intent intent = new Intent(getApplicationContext(),
                com.example.die_dreisten_drei.an2ro.backend_gedoens.PlayerService.class);
        String intentURL = url;
        intent.putExtra("INTENT_URL",url).putExtra("PODCAST_LISTPOSITION",PODCAST_LISTPOSITION).putExtra("PODCAST_ITEMLISTPOSITION", podcastItemPosition);

        startService(intent);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals(MY_ACTION)){
                String message = intent.getStringExtra(MY_ACTION);
                            }
            else if(intent.getAction().equals(INVALID_URL)){
                String message = intent.getStringExtra(INVALID_URL);
            }
        }
    };


    private void initializeViews() {
        btnPlay = findViewById(R.id.btnPlay);
        btnPause = findViewById(R.id.btnPause);
        btnDownload = findViewById(R.id.btnDownload);
        btnSkipPrev = findViewById(R.id.btnSkipPrev);
        if(podcastItemPosition==0)
            btnSkipPrev.setEnabled(false);
        btnSkipNext=findViewById(R.id.btnSkipNext);
        if(podcastItemPosition==podCast.getStreams().size()-1)
            btnSkipNext.setEnabled(false);
        tfTitle = findViewById(R.id.tfTitle);
        tfTitle.setText(podCast.getStreams().get(podcastItemPosition).getTitle());
        imageIcon = findViewById(R.id.imageIcon);

        Picasso.get()
                .load("file://"+podCast.getImage())
                .placeholder(R.drawable.placeholder_image)
                .fit()
                .into(imageIcon);
				
        tfNextTitle = findViewById(R.id.tfNextTitle);
        if(podCast.getStreams().get(podcastItemPosition+1)!=null && podCast.getStreams().get(podcastItemPosition+1).getTitle().length()>29)
            tfNextTitle.setText(podCast.getStreams().get(podcastItemPosition+1).getTitle().substring(0,30)+"...");
        else
            tfNextTitle.setText(podCast.getStreams().get(podcastItemPosition+1).getTitle());
        tfNextDesc = findViewById(R.id.tfNextDesc);
        tfNextDesc.setMovementMethod(new ScrollingMovementMethod());
        if(podCast.getStreams().get(podcastItemPosition+1)!=null)
            tfNextDesc.setText(podCast.getStreams().get(podcastItemPosition+1).getDescription());
        seekBar = findViewById(R.id.seekBar);
        tfDescription = findViewById(R.id.tfDescription);
        tfDescription.setText(podCast.getStreams().get(podcastItemPosition).getDescription());
        tfDescription.setMovementMethod(new ScrollingMovementMethod());
        tfPlayTime = findViewById(R.id.tfPlayTime);
    }

    public void onClickRewind(View v) {
        EventBus.getDefault().post(new MessageEvent(getString(R.string.eb_seekback)));
        updateTime();
    }


    public void onClickForward(View v) {
        EventBus.getDefault().post(new MessageEvent(getString(R.string.eb_seekforward)));
        updateTime();
    }

    public void onClickPlay(View v) {
        isPlaying=true;
        EventBus.getDefault().post(new MessageEvent(getString(R.string.eb_startpodcast)));
        btnPlay.setVisibility(View.INVISIBLE);
        btnPause.setVisibility(View.VISIBLE);
        updateTime();

        // Notification, die den Titel eines spielenden Podcasts anzeigt
        // todo Player Control Buttons
        if (SettingsActivity.notificationStatus) {
            //Notification Gedöns
            String textTitle = "An2Ro - gerade spielt: ";
            String textContent = (String) tfTitle.getText().toString();
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "1337")
                    .setSmallIcon(R.drawable.logo_v4)
                    .setContentTitle(textTitle)
                    .setContentText(textContent)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setOngoing(true);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            // notificationId is a unique int for each notification that you must define
            notificationManager.notify(42, mBuilder.build());
        }
    }

    public void skipPrev(View v){
       // stopService(i);
        PlayerServiceObj.releaseMediaPlayer();
        startActivity(new Intent(this, PlayerActivity.class).putExtra("PODCAST", PODCAST_LISTPOSITION).putExtra("ITEMPOSITION", podcastItemPosition-1));
    }

    public void skipNext(View v){
        PlayerServiceObj.releaseMediaPlayer();
        startActivity(new Intent(this, PlayerActivity.class).putExtra("PODCAST", PODCAST_LISTPOSITION).putExtra("ITEMPOSITION", podcastItemPosition+1));
    }

    public void onClickDownload(View v) {
        //ToDo: deaktivieren, wenn oldUrl = null ist
        //pseudocode: if(podcastItem.oldUrl == null){button deaktivieren}
//        if(podcastItem.getOld_url()==null)
//        {
//            btnDownload.setActivated(false);
//        }
//        else{
//            btnDownload.setActivated(true);
//        }
        EventBus.getDefault().post(new MessageEvent(getString(R.string.eb_downloadpodcast)));

    }


    public void onClickPause(View v) {
        //            if (mediaPlayer.isPlaying()) {

        //löscht die Notification, wenn der Player pausiert wird
        if (SettingsActivity.notificationStatus) {
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            notificationManager.cancel(42);
        }

        updateTime();
        if(isPlaying){
            btnPlay.setVisibility(View.VISIBLE);
            btnPause.setVisibility(View.INVISIBLE);
            EventBus.getDefault().post(new MessageEvent(getString(R.string.eb_pausepodcast)));

            isPlaying = false;
        }else if(!isPlaying){
            btnPlay.setVisibility(View.INVISIBLE);
            btnPause.setVisibility(View.VISIBLE);
            isPlaying = true;
            EventBus.getDefault().post(new MessageEvent(getString(R.string.eb_startpodcast)));
        }
    }



    private static String getInTime(int ms) {
        int seconds = (int) ((ms) / 1000)%60;
        int hours = (int) ms / 3600000;
        int minutes = ((int) ms / 60000)-hours*60;
        
        // korrektes format 00:00:00
        String newSeconds = "" + seconds;
        String newHours = "" + hours;
        String newMinutes = "" + minutes;
        if (seconds < 10) {
            newSeconds = "0" + seconds;
        }
        if (hours < 10) {
            newHours = "0" + hours;
        }
        if (minutes < 10) {
            newMinutes = "0" + minutes;
        }

        return newHours + ":" + newMinutes + ":" + newSeconds;
    }

    //setze Zeit & aktualisiere diese
    private void updateTime() {

            intPodcastCurrentPosi = ((PlayerServiceObj.getCurrentPosition()) / 1000);
            seekBar.setProgress(intPodcastCurrentPosi);
            currentPosition = PlayerServiceObj.getCurrentPosition();
            currentLength = PlayerServiceObj.getLength();
            tfPlayTime.setText(getInTime(PlayerServiceObj.getCurrentPosition()) + " / " + getInTime(PlayerServiceObj.getLength()));
        }


    //Was soll passieren, wenn connectet / disconnectet wird?
    private ServiceConnection PlayerServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            //Referenz auf MyLocaleBinder --> Access granten
            MyLocaleBinder binder = (MyLocaleBinder) service;

            //Nun: getService-Methode callen, um Service nutzen zu können
            PlayerServiceObj = binder.getService();
            isBound = true;

            if(PlayerServiceObj!=null){

                podcastLaenge = String.valueOf(PlayerServiceObj.getLength());
                podcastCurrentPosi = String.valueOf(PlayerServiceObj.getCurrentPosition());
                intPodcastLaenge = (PlayerServiceObj.getLength());
            }

            intPodcastLaenge= intPodcastLaenge/1000;
            seekBar.setMax(intPodcastLaenge);
            seekUpdation();

        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            PlayerServiceConnection = null;
            isBound = false;
        }
    };


    //NavigationDrawer-Gedöns
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Menu menuNav = navigationView.getMenu();
        MenuItem nav_player = menuNav.findItem(R.id.nav_player);
        nav_player.setEnabled(false);
        return true;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.player_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        Log.d("onback", "getLastPosi: " + podcastItem.getLastPosition() );

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_podcasts) {
            Intent podcastIntent = new Intent(this, MainActivity.class);
            startActivity(podcastIntent);
        } else if (id == R.id.nav_player) {

        } else if (id == R.id.nav_offlinePodcasts) {
            Intent toDownloadedfiles = new Intent(this, Downloadedfiles.class);
            startActivity(toDownloadedfiles);
        } else if (id == R.id.nav_settings) {
            Intent toSettings = new Intent(this, SettingsActivity.class);
            startActivity(toSettings);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.player_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    Runnable run = new Runnable() {

        @Override
        public void run() {
            seekUpdation();
        }
    };

    public void seekUpdation() {
        updateTime();
        seekHandler.postDelayed(run, 1000);
    }


    public void setupSeekbar() {

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            // This holds the progress value for onStopTrackingTouch.
            int userSelectedPosition = seekBar.getProgress();

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

               //  Only fire seekTo() calls when user stops the touch event.
                if (fromUser) {
                    userSelectedPosition = progress*1000;
                    Log.d("seekposi", "userSelectedPosition: " + userSelectedPosition);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekingPodcast = false;
                EventBus.getDefault().post(new SeekbarEvent.SeekTo(
                        userSelectedPosition));
            }
        });

    }


}
