package com.example.die_dreisten_drei.an2ro.backend_gedoens;

import java.io.Serializable;
import java.util.ArrayList;

/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */

public class ItemList<Podcast> extends ArrayList implements Serializable {


    public Podcast get(int i){
       return (Podcast) super.get(i);
    }
}
