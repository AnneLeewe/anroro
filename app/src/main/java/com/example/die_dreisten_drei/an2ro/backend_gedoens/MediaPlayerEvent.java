package com.example.die_dreisten_drei.an2ro.backend_gedoens;

/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */
public class MediaPlayerEvent {


        public static class UpdateLog {

            public final StringBuffer formattedMessage;

            public UpdateLog(StringBuffer formattedMessage) {
                this.formattedMessage = formattedMessage;
            }
        }

        public static class PlaybackDuration {

            public final int duration;

            public PlaybackDuration(int duration) {
                this.duration = duration;
            }
        }

        public static class PlaybackPosition {

            public final int position;

            public PlaybackPosition(int position) {
                this.position = position;
            }
        }

        public static class PlaybackCompleted {

        }

//        public static class StateChanged {
//
//            public final PlayerService.PlayerState currentState;
//
//            public StateChanged(MediaPlayerHolder.PlayerState currentState) {
//                this.currentState = currentState;
//            }
//        }
}