package com.example.die_dreisten_drei.an2ro;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.die_dreisten_drei.an2ro.backend_gedoens.Podcast;
import com.example.die_dreisten_drei.an2ro.backend_gedoens.RSSRequest;
import com.squareup.picasso.Picasso;

import org.xml.sax.SAXException;

import java.io.IOException;

/** @author Robert Pfeiffer, Robert Neumann, Anne-Kathrin Leewe
 */
public class ChannelActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SwipeRefreshLayout.OnRefreshListener {
    ListView list ;
    private ImageView icon;
    private TextView tfTitle;
    private TextView tfDesc;
    static int Podcastid =0;
    private ChannelActivity activity = this;

    private ChannelAdapter pcItems;
    private Podcast channel;
    private Intent startIntent;

    public static String searchString = "";

    public NavigationView navigationView;
    private DrawerLayout mDrawerLayout;
    private SwipeRefreshLayout mSwipeRefreshLayout;


    @Override
    public void onRefresh() {
        try {
            new RSSRequest(channel.getUrl(),channel);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        Toast.makeText(this, "Podcast-Liste aktualisiert", Toast.LENGTH_LONG).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                viewAktualisieren();
            }
        }, 2000);
        viewAktualisieren();
    }

    public void viewAktualisieren(){
        //ToDo: Channelliste aktualisieren. Also wirklich jetzt.
        try {
            new RSSRequest(channel.getUrl(),channel);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        pcItems = new ChannelAdapter(this, channel.getStreams(), getResources());
        list.setAdapter(pcItems);
        listIsAtTop();
        mSwipeRefreshLayout.setRefreshing(false);
    }
    private boolean listIsAtTop()   {
        if(list.getChildCount() == 0) return true;
        return list.getChildAt(0).getTop() == 0;
    }

    private void initializeViews(){
        icon = findViewById(R.id.imageChannelIcon);

        Picasso.get().load("file://"+channel.getImage())
                .placeholder(R.drawable.placeholder_image)
                .fit()
                .into(icon);

        tfTitle=findViewById(R.id.tfChannelTitle);

        tfTitle.setText(channel.getTitle());
        tfDesc=findViewById(R.id.tfChannelDescription);
        tfDesc.setText(channel.getDesc());
        tfDesc.setMovementMethod(new ScrollingMovementMethod());
        list = findViewById(R.id.ChannelList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.baseline_menu_white_18dp);

        mSwipeRefreshLayout = findViewById(R.id.container);
        mSwipeRefreshLayout.setColorScheme(R.color.blue,
                R.color.green, R.color.orange, R.color.purple);
        mSwipeRefreshLayout.setOnRefreshListener(this);


        navigationView = (NavigationView) findViewById(R.id.nav_view2);
        navigationView.setNavigationItemSelectedListener(this);

        mDrawerLayout = findViewById(R.id.channel_drawer_layout);

        startIntent = getIntent();
        Podcastid=(int) startIntent.getIntExtra("PODCAST",0);
        channel = MainActivity.aboList.get(Podcastid);
        initializeViews();
        pcItems = new ChannelAdapter(this, channel.getStreams(), getResources());

        list.setAdapter(pcItems);

        /*
        * Funktion, um swipeToRefresh in der ListView zu ermöglichen; prueft, ob man am Beginn der Liste ist*/
        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(firstVisibleItem == 0 && listIsAtTop()){
                    mSwipeRefreshLayout.setEnabled(true);
                }else{
                    mSwipeRefreshLayout.setEnabled(false);
                }
            }
        });

    }


    public void onItemClick(int mPosition) {
        startActivity(new Intent(this, PlayerActivity.class).putExtra("PODCAST", startIntent.getSerializableExtra("PODCAST")).putExtra("ITEMPOSITION", mPosition));


    }


    //SearchWidget zum Filtern innerhalb der Activity
    //Sources: https://developer.android.com/training/appbar/action-views
    //https://javapapers.com/android/android-searchview-action-bar-tutorial/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        // Inflate menu to add items to action bar if it is present.
        inflater.inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Was passiert bei Textübergabe (nachdem Enter/Suchlupe gedrückt wurde)

                //Query String, der im Moment im SearchView steht
                searchString = (String) searchView.getQuery().toString();
                pcItems = new ChannelAdapter(activity, channel.getStreams(), getResources());
                list.setAdapter(pcItems);
                /*aboAdapter = new MainAdapter(activity, aboList, getResources());
                list.setAdapter(aboAdapter);*/
                // Test Ausgabe als Toast
//                Toast.makeText(getApplicationContext(), searchString, Toast.LENGTH_SHORT).show();



                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // was passiert, wenn der Text geändert wird
                // hier müsste dann die Suchfunktion rein, wenn wir das als "live search" machen wöllten

                if(((String) searchView.getQuery().toString()).isEmpty()) {
                    searchString = "";
                    pcItems = new ChannelAdapter(activity, channel.getStreams(), getResources());
                    list.setAdapter(pcItems);
                    /*aboAdapter = new MainAdapter(activity, aboList, getResources());
                    list.setAdapter(aboAdapter);*/

                }
                return false;
            }
        });


        return true;
    }

    //NavigationDrawer Gedöns

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.channel_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_podcasts) {
            Intent podcastIntent = new Intent(this, MainActivity.class);
            startActivity(podcastIntent);
        } else if (id == R.id.nav_player) {

        } else if (id == R.id.nav_offlinePodcasts) {
            Intent toDownloadedfiles = new Intent(this, Downloadedfiles.class);
            startActivity(toDownloadedfiles);
        } else if (id == R.id.nav_settings) {
            Intent toSettings = new Intent(this, SettingsActivity.class);
            startActivity(toSettings);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.channel_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
